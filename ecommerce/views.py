from django.shortcuts import render
from pip._vendor import requests
from products.models import Product
from .forms import ContactForm
from django.http import JsonResponse, HttpResponse
from orders.models import Order
from billing.models import BillingProfile
from billing.models import BillingProfile
from django.http import HttpResponse

def home_page(request):
    context = {}
    product_obj = Product.objects.all()
    context['products'] = product_obj
    return render(request, "home.html", context)


def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        'form': contact_form
    }
    secret_key = '6Lct81saAAAAAIUjMBgvUGGPR1Jyx59csYMXjNee'
    data = {
        'response': request.POST.get('g-recaptcha-response'),
        'secret': secret_key
    }
    resp = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
    result_json = resp.json()
    if request.is_ajax() and result_json.get('success'):
        if contact_form.is_valid():
            return JsonResponse({'message': 'Thank you.'}, status=200)
        if contact_form.errors:
            return HttpResponse(contact_form.errors.as_json(), status=400, content_type='application/json')
    return render(request, "contact.html", context)


def profile_page(request):
    context = {}
    current_user=request.user
    bill_profile=BillingProfile.objects.filter(user_id = current_user.id).first()
    context['orders']=Order.objects.filter(billing_profile_id = bill_profile.id)
    return render(request, "profile.html", context)
