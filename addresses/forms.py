from django import forms

from .models import Address


class AddressForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
       super(AddressForm, self).__init__(*args, **kwargs)
       self.fields['country'].widget.attrs['id'] = 'countryId'    
       self.fields['state'].widget.attrs['id'] = 'stateId'    
       self.fields['city'].widget.attrs['id'] = 'cityId'    
    class Meta:
        model = Address
        fields = [
            'address_line_1',
            'address_line_2',
            'country',
            'state',
            'city',
            'postal_code',
            'phone_number'
        ]
