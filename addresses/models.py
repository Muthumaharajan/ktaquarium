from django.db import models
from billing.models import BillingProfile


ADDRESS_TYPES = (
    ('billing', 'Billing'),
    ('shipping', 'Shipping')
)


class Address(models.Model):
    billing_profile = models.ForeignKey(BillingProfile, null=True, blank=False, on_delete=models.SET_NULL)
    address_type = models.CharField(max_length=120, choices=ADDRESS_TYPES)
    address_line_1 = models.CharField(max_length=120)
    address_line_2 = models.CharField(max_length=120, null=True, blank=True)
    country = models.CharField(max_length=120, default='India')
    state = models.CharField(max_length=120)
    city = models.CharField(max_length=120)
    postal_code = models.CharField(max_length=120)
    phone_number = models.CharField(max_length=10, null=True, blank=True, default=0)

    def __str__(self):
        return str(self.billing_profile) + ' : ' + str(self.address_type).upper()

    def get_address(self):
        return f"{self.address_line_1} {self.address_line_2 or ''} / {self.state}, {self.city} {self.postal_code} {self.country}"