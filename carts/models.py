from django.conf import settings
from django.db import models
from products.models import Product
from django.db.models.signals import pre_save, m2m_changed
from decimal import Decimal

User = settings.AUTH_USER_MODEL


class CartManager(models.Manager):
    def new_or_get(self, request):
        cart_id = request.session.get('cart_id', None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.count() == 1:
            new_obj = False
            cart_obj = qs.first()
            if request.user.is_authenticated and cart_obj.user is None:
                cart_obj.user = request.user             
                cart_obj.save()
        else:
            cart_obj = self.new(user=request.user)
            new_obj = True
            request.session['cart_id'] = cart_obj.id
        return cart_obj, new_obj

    def new(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated:
                user_obj = user
        return self.model.objects.create(user=user_obj)
class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    products = models.ManyToManyField(Product, blank=True)
    subtotal = models.DecimalField(max_digits=30, decimal_places=4, default=0)
    total = models.DecimalField(max_digits=30, decimal_places=4, default=0)
    receipt = models.ImageField(upload_to="cart", height_field=None, width_field=None, max_length=100, default="")
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = CartManager()

    def __str__(self):
        return str(self.id)

class ProductCount(models.Model):
    cart_id = models.ForeignKey(Cart, on_delete=models.SET_NULL, null=True, blank=True)
    product_id = models.ForeignKey(Product, verbose_name="Product", on_delete=models.SET_NULL, null=True, blank=True)
    count = models.IntegerField(default=1)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)


def m2m_changed_cart_receiver(sender, instance, action, *args, **kwargs):
    if action == 'post_add' or action == 'post_remove' or action == 'post_clear':
        products = instance.products.all()        
        total = 0
        for x in products:
            try:
                if instance.productcount_set.filter(product_id=x.id).get():
                    product_count = instance.productcount_set.filter(product_id=x.id).get()
                    total += x.price * product_count
            except instance.productcount_set.DoesNotExist:
                total += x.price
        if instance.subtotal != total:
            instance.subtotal = total
            instance.save()


m2m_changed.connect(m2m_changed_cart_receiver, sender=Cart.products.through)


def pre_save_cart_receiver(sender, instance, *args, **kwargs):
    if instance.subtotal > 0:
        instance.total = Decimal(instance.subtotal) * Decimal(1.08)
    else:
        instance.total = 0


pre_save.connect(pre_save_cart_receiver, sender=Cart)

