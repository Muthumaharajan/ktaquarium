from django import forms
from .models import Cart


class ImageForm(forms.ModelForm):
    """Form for the image model"""
    class Meta:
        model = Cart
        fields = [
                'receipt'
                ]
