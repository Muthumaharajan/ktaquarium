from django.http import JsonResponse
from django.shortcuts import render, redirect
from products.models import Product
from .models import Cart, ProductCount
from orders.models import Order
from accounts.forms import LoginForm, GuestForm
from billing.models import BillingProfile
from addresses.forms import AddressForm
from addresses.models import Address
import json
from decimal import Decimal
from django.db.models import Q
from django.http import HttpResponse
from carts.forms import ImageForm
from django.core.mail import send_mail
import os
from django.conf import settings

def cart_detail_api_view(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = [{
        'id': x.product_id.id,
        'url': x.product_id.get_absolute_url(),
        'title': x.product_id.title,
        'price': format(x.product_id.price, '.2f')
    } for x in cart_obj.productcount_set.all()]
    cart_data = {
        'products': products,
        'subTotal': format(cart_obj.subtotal, '.2f'),
        'total': format(cart_obj.total, '.2f')
    }
    return JsonResponse(cart_data, status=200)


def cart_home(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    return render(request, "carts/home.html", {"cart": cart_obj})


def cart_update(request):
    product_id = request.POST.get('product_id')
    cart_id = request.POST.get('cart_id')
    qty = request.POST.get('qty')
    if product_id is not None:

        # get product or return cart page
        try:
            product_obj = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            # ajax error response
            if request.is_ajax():
                json_data = {
                    'added': False,
                    'removed': False,
                }
                return JsonResponse(json_data, status=409)

            return redirect("carts:home")

        cart_obj, new_obj = Cart.objects.new_or_get(request)

        if qty is not None:
            obj = ProductCount.objects.get(cart_id=cart_id, product_id=Product.objects.get(id=product_id))
            if obj:
                obj.count = qty
                obj.save()
            product_added = True
        elif product_obj in cart_obj.products.all():
            # remove product cart
            cart_obj.products.remove(product_obj)
            cart_obj.productcount_set.filter(product_id=product_obj.id).delete()
                
            product_added = False
        else:
            # add product cart3
            cart_obj.products.add(product_obj)
            ProductCount.objects.create(cart_id=cart_obj, product_id=Product.objects.get(id=product_id), count=1)
            product_added = True

        # cart product count update
        request.session['cart_items'] = cart_obj.products.count()
        if request.is_ajax():
            json_data = {
                'added': product_added,
                'removed': not product_added,
                'cartItemCount': cart_obj.products.count(),
            }
            return JsonResponse(json_data, status=200)

    # no product_id in post request
    return redirect("carts:home")


def checkout_home(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    order_obj = None
    billing_address_id = request.session.get('billing_address_id', None)
    shipping_address_id = request.session.get('shipping_address_id', None)

    if cart_created or cart_obj.products.count() == 0:
        return redirect("carts:home")

    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
    address_qs = None
    if billing_profile is not None:
        if request.user.is_authenticated:
            address_qs = Address.objects.filter(billing_profile=billing_profile)
        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)

        if shipping_address_id:
            order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
            del request.session['shipping_address_id']
        if billing_address_id:
            order_obj.billing_address = Address.objects.get(id=billing_address_id)
            del request.session['billing_address_id']
        if billing_address_id or shipping_address_id:
            order_obj.save()
           
    if request.method == 'POST':
        "some check that order is done"
        is_done = order_obj.check_done()
        if is_done:
            order_obj.mark_paid()
            request.session['cart_items'] = 0
            del request.session['cart_id']
            request.session['order_code'] = request.POST.get('order_code')
            subject = 'order'
            message = f'Hi {request.user.username}, thank you for order.'
            email_from = settings.EMAIL_HOST_USER
            recipient_list = [request.user.email, ]
            send_mail( subject, message, email_from, recipient_list )   

            return redirect("carts:success")

    context = {
        'object': order_obj,
        'billing_profile': billing_profile,
        'login_form': LoginForm(),
        'guest_form': GuestForm(),
        'address_form': AddressForm(),
        'address_qs': address_qs,
    }

    return render(request, "carts/checkout.html", context)


def checkout_done_view(request):
    context = {
        "order_code": request.session['order_code']
    }
    return render(request, "carts/checkout-done.html", context)

IMAGE_FILE_TYPES = ['png', 'jpg', 'jpeg']
def cart_image_upload(request):    
    if request.method == 'POST': 
        cart_id = request.POST.get('cart_id',"") 
        old = Cart.objects.get(id=cart_id)
        form = ImageForm(request.POST, request.FILES, instance=old)
        if form.is_valid():         
            form.save()
            return JsonResponse({'error': False, 'message': 'Uploaded Successfully'})
        else:
           return JsonResponse({'error': True, 'errors': form.errors})