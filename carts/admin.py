from django.contrib import admin
from .models import Cart, ProductCount

class ProductCountAdmin(admin.ModelAdmin):
      list_display = ['cart_id', 'product_id','count']
      fields = ['count', 'product_id', 'cart_id']

class ProductCountInline(admin.TabularInline):
    model = ProductCount
class CartAdmin(admin.ModelAdmin):
    model = Cart
    inlines = [
        ProductCountInline,
    ]

admin.site.register(Cart,CartAdmin)
admin.site.register(ProductCount,ProductCountAdmin)