from django.contrib import admin
from .models import Order
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin

@admin.register(Order)
class OrderAdmin(ImportExportModelAdmin):
    pass
