import json

from django.db.models.fields import Field
from import_export import resources
from .models import Order,BillingProfile,Address,Cart
from django.core import serializers
from import_export.widgets import JSONWidget, ManyToManyWidget, ForeignKeyWidget
class OrderResource(resources.OrderResource):
    class Meta:
        model = Order
        skip_unchanged = True
        report_skipped = True
        # import_id_fields = ('edx_id', 'edx_email', 'edx_name',)
        exclude = ('created_at', 'updated_at', 'id',)